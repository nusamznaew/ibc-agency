<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Title -->
    <title>IBC Agency | @yield('title', '')</title>

    <!--  Bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">

    <!-- Main Css -->
    <link rel="stylesheet" href="{{ mix('assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}">

    <!-- Extra Css -->
    @yield('extra-css')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://unpkg.co/gsap@3/dist/gsap.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	@yield('pixel')
</head>
<body class='@yield('cursor', '')'>
{{--<div id="loading"></div>--}}
<div class='@yield('page', '')'>
    @yield('content')
    @include('partials.footer')
</div>
@include('partials.modal')
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/gsap.min.js"
        integrity="sha512-eP6ippJojIKXKO8EPLtsUMS+/sAGHGo1UN/38swqZa1ypfcD4I0V/ac5G3VzaHfDaklFmQLEs51lhkkVaqg60Q=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/blazy@1.8.2/blazy.min.js"
        integrity="sha256-D0J9D4igaYyVX/Y78Tr0yoDJsy8hi14hCEdFDakBp08=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
    AOS.init();
</script>
@if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
    <script>
        $(function () {
            $('#modalSuccess').modal('show');
        });
    </script>
@endif
@yield('extra-js')
<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset("assets/js/jquery.maskedinput.js") }}"></script>
<script>
    $(".phone").mask("+7(999)-999-99-99");
</script>
<script>
    function validate(input) {
        if (/^\s/.test(input.value))
            input.value = '';
    }
</script>
<script>
    $(".phone").change(function () {
        if ($(".phone").val() === "+7(123)-456-78-90" ||
            $(".phone").val() === "+7(000)-000-00-00" ||
            $(".phone").val() === "+7(111)-111-11-11" ||
            $(".phone").val() === "+7(222)-222-22-22" ||
            $(".phone").val() === "+7(333)-333-33-33" ||
            $(".phone").val() === "+7(444)-444-44-44" ||
            $(".phone").val() === "+7(555)-555-55-55" ||
            $(".phone").val() === "+7(666)-666-66-66" ||
            $(".phone").val() === "+7(777)-777-77-77" ||
            $(".phone").val() === "+7(888)-888-88-88" ||
            $(".phone").val() === "+7(999)-999-99-99") {
            $(".phone").val("");
        }
    });
</script>
<script>
	function reCAPTCHA() {
		var registerBtn = document.querySelector('#registerBtn');
		registerBtn.removeAttribute('disabled');
		registerBtn.style.cursor = 'pointer';
	}
</script>
<script>
    $(document).ready(function() {
        $(document).on('submit', 'form', function() {
            $('.submit-button').attr('disabled', 'disabled').css('cursor', 'not-allowed');
        });
    });
</script>	
</body>
</html>
