@extends('layouts.layout')

@section('title', 'Кейсы')

@section('cursor', 'black')

@section('page', 'page-white')

@section('extra-css')

@endsection

@section('pixel')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '656243942042560');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
@endsection

@section('content')
    @include('partials.dark-header')
    <div class="container page-cases">
        <div class="row">
            <div class="col-lg-6 col-12">
                <h2>Кейсы</h2>
            </div>
            <div class="col-lg-6 col-12 d-flex align-items-end">
                <p class="small-txt">Мы разрабатываем стратегии и креатив,
                    запускаем рекламные кампании и реализуем
                    масштабные проекты, которые приносят
                    финансовый результат нашим клиентам.</p>
            </div>
        </div>
        <div class="block-2 d-flex flex-column" id="cases">
            <div class="d-flex flex-lg-row flex-column align-items-lg-stretch align-items-center justify-content-between top">
                <a href="/works/podnyali-pribyl-kompanii-v-2-raza-uzhe-v-pervyj-mesyac-vedeniya-targetirovannoj-reklamy-v-instagram" class="d-flex flex-column justify-content-end case1">
                    <video playsinline autoplay muted loop class="gif gif1 position-absolute"
                           src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251421/video1_rrlxel.mp4"></video>
                    <p>Программа "Work <br> and Study USA"</p>
                </a>
                <div class="d-flex flex-column justify-content-between center">
                    <a href="/works/captain-campus" class="d-flex flex-column justify-content-end case2">
                        <video playsinline autoplay muted loop class="gif gif2 position-absolute"
                               src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/video2_aogbm7.mp4"></video>
                        <p>Конкурс на бесплатную подготовку <br> к поступлению в США</p>
                    </a>
                    <a href="/works/pomogli-prodvinut-v-socsetyah-uslugu-socialnoj-adaptacii-v-ssha" class="d-flex flex-column justify-content-end case3">
                        <video playsinline autoplay muted loop class="gif gif3 position-absolute"
                               src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/video3_hzdwej.mp4"></video>
                        <p>Услуги социальной адаптации в США</p>
                    </a>
                </div>
                <div class="d-flex flex-column justify-content-between">
                    <a href="/works/za-schet-targetirovannoj-reklamy-smogli-privlech-klientov-v-shkolu-anglijskogo-yazyka-v-almaty-i-poluchili-neobhodimoe-kolichestvo-uchenikov-na-ves-god" class="d-flex flex-column justify-content-end case4">
                        <img src="{{ asset('video/video4.gif') }}" class="gif gif4 position-absolute" alt="">
                        <p>Языковые курсы</p>
                    </a>
                    <a href="/works/zapustili-onlajn-kurs-sistematizaciya-biznesa-ot-idei-do-realizacii-v-sfere-biznes-obrazovaniya" class="d-flex flex-column justify-content-end case5">
                        <video playsinline autoplay muted loop class="gif gif5 position-absolute"
                               src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251420/video5_rs3ae6.mp4"></video>
                        <p>Образовательный бизнес курс</p>
                    </a>
                </div>
            </div>
            <div class="d-flex flex-lg-row flex-column align-items-lg-stretch align-items-center">
                <a href="/works/sozdali-potok-klientov-na-uchastie-v-mezhdunarodnom-forume-v-g-turkestane" class="d-flex flex-column justify-content-end case6">
                    <video playsinline autoplay muted loop class="gif gif6 position-absolute"
                           src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/mezh_eoatvs.mp4"></video>
                    <p>Международный форум <br> «1 000 000$»</p>
                </a>
                <div class="d-flex flex-column justify-content-between center2">
                    <a href="/works/sdelali-dlya-blagotvoritelnogo-fonda-obzornoe-video-uchastiya-sportsmenov-s-ogranichennymi-vozmozhnostyami-v-chempionate-kazahstana-po-bodibildingu" class="d-flex flex-column justify-content-end case7">
                        <video playsinline autoplay muted loop class="gif gif7 position-absolute"
                               src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/par_b4d47z.mp4"></video>
                        <p>Видео про паралимпийцев</p>
                    </a>
                    <a href="/works/sozdali-s-nulya-kanal-dlya-predprinimatelya-i-sdelali-zapis-pervyh-2-h-intervyu" class="d-flex flex-column justify-content-end case8">
                        <video playsinline autoplay muted loop class="gif gif8 position-absolute"
                               src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/kyr_barbjz.mp4"></video>
                        <p>Продвижение канала <br> на Youtube</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $("#case1").hide();
            $("#case2").hide();
            $("#case3").hide();
            $("#case4").hide();
            $("#case5").hide();
            $(".case1").hover(function () {
                    $("#case1").show();
                    $(document).mousemove(function (e) {
                        $("#case1").css({
                            left: e.pageX,
                            top: e.pageY
                        });
                    });
                },
                function () {
                    $("#case1").hide();
                });

            $(".case2").hover(function () {
                    $("#case2").show();
                    $(document).mousemove(function (e) {
                        $("#case2").css({
                            left: e.pageX,
                            top: e.pageY
                        });
                    });
                },
                function () {
                    $("#case2").hide();
                });

            $(".case3").hover(function () {
                    $("#case3").show();
                    $(document).mousemove(function (e) {
                        $("#case3").css({
                            left: e.pageX,
                            top: e.pageY
                        });
                    });
                },
                function () {
                    $("#case3").hide();
                });

            $(".case4").hover(function () {
                    $("#case4").show();
                    $(document).mousemove(function (e) {
                        $("#case4").css({
                            left: e.pageX,
                            top: e.pageY
                        });
                    });
                },
                function () {
                    $("#case4").hide();
                });

            $(".case5").hover(function () {
                    $("#case5").show();
                    $(document).mousemove(function (e) {
                        $("#case5").css({
                            left: e.pageX,
                            top: e.pageY
                        });
                    });
                },
                function () {
                    $("#case5").hide();
                });
        });
    </script>
@endsection
