@extends('layouts.layout')

@section('title', 'Карьера')

@section('cursor', 'black')

@section('page', 'page-white')

@section('extra-css')

@endsection

@section('pixel')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '656243942042560');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
@endsection

@section('content')
    @include('partials.dark-header')
    <div class="page-vacancy">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <h2 class="header">Карьера</h2>
                </div>
                <div class="col-lg-6 col-12 d-flex align-items-end">
                    <p class="small-txt">Люди - главная ценность компании. Мы верим в
                        каждого. У нас люди находят себя и свое
                        призвание. А мы вдохновляем новыми
                        горизонтами и поддерживаем во всех начинаниях.</p>
                </div>
            </div>
        </div>
        <div class="vacancy">
            <div class="container">
                <div class="header d-flex align-items-center">
                    <p class="header">Мы ищем:</p>
                </div>
            </div>
            <div class="vacancies">
                <div class="accordion" id="accordionVacancy">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading1">
                            <a class="accordion-button collapsed" type="button"
                               data-bs-toggle="collapse"
                               data-bs-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                <div class="container">
                                    <div
                                        class="d-flex flex-lg-row flex-column justify-content-between align-items-lg-center align-items-start content">
                                        <h3>Copywriter</h3>
                                        <div class="d-flex right align-items-center">
                                            <p class="description">Мы ищем специалиста, который понимает геометрию букв
                                                и грамотно может написать историю жизни человека.</p>
                                            <svg version="1.1" width="80" height="80" viewBox="0 0 80 80" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="40" cy="40" r="39.5"/>
                                                <path
                                                    d="M37.992 42.264H29.016V38.904H37.992V29.208H41.496V38.904H50.472V42.264H41.496V51.96H37.992V42.264Z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </h2>
                        <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1"
                             data-bs-parent="#accordionVacancy">
                            <div class="accordion-body container">
                                <div class="d-flex flex-lg-row flex-column detailed">
                                    <p class="big-txt">
                                        Что нужно делать
                                    </p>
                                    <ul>
                                        <li>
                                            Контролировать качество работы копирайтеров на проектах (стратегии и
                                            smm-соспровождение), результаты работы группы и KPI.
                                        </li>
                                        <li>
                                            Контролировать и распределять загрузку группы.
                                        </li>
                                        <li>
                                            Контролировать рентабельность группы.
                                        </li>
                                        <li>
                                            Пальпировать настроения и пожелания группы.
                                        </li>
                                        <li>
                                            Работать с мотивацией группы: разрабатывать финансовые и нефинансовые
                                            системы мотиваций.
                                        </li>
                                        <li>
                                            Супервайзить стратегии, участвовать в штурмах, давать своевременную обратную
                                            связь.
                                        </li>
                                        <li>
                                            Составлять планы личного развития команды.
                                        </li>
                                        <li>
                                            Поиск, онбординг и адаптация команды копирайтеров.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading2">
                            <a class="accordion-button collapsed" type="button"
                               data-bs-toggle="collapse"
                               data-bs-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                <div class="container">
                                    <div
                                        class="d-flex flex-lg-row flex-column justify-content-between align-items-lg-center align-items-start content">
                                        <h3>Tik-Tok Creator</h3>
                                        <div class="d-flex right align-items-center">
                                            <p class="description">Мы в поисках начинающего специалиста, который хочет
                                                развиваться в самой креативной социальной сети, который не боится
                                                предлагать
                                                идеи и защищать их перед клиентом.</p>
                                            <svg version="1.1" width="80" height="80" viewBox="0 0 80 80" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="40" cy="40" r="39.5"/>
                                                <path
                                                    d="M37.992 42.264H29.016V38.904H37.992V29.208H41.496V38.904H50.472V42.264H41.496V51.96H37.992V42.264Z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </h2>
                        <div id="collapse2" class="accordion-collapse collapse" aria-labelledby="heading2"
                             data-bs-parent="#accordionVacancy">
                            <div class="accordion-body container">
                                <div class="d-flex flex-lg-row flex-column detailed">
                                    <p class="big-txt">
                                        Что нужно делать
                                    </p>
                                    <ul>
                                        <li>
                                            Контролировать качество работы копирайтеров на проектах (стратегии и
                                            smm-соспровождение), результаты работы группы и KPI.
                                        </li>
                                        <li>
                                            Контролировать и распределять загрузку группы.
                                        </li>
                                        <li>
                                            Контролировать рентабельность группы.
                                        </li>
                                        <li>
                                            Пальпировать настроения и пожелания группы.
                                        </li>
                                        <li>
                                            Работать с мотивацией группы: разрабатывать финансовые и нефинансовые
                                            системы мотиваций.
                                        </li>
                                        <li>
                                            Супервайзить стратегии, участвовать в штурмах, давать своевременную обратную
                                            связь.
                                        </li>
                                        <li>
                                            Составлять планы личного развития команды.
                                        </li>
                                        <li>
                                            Поиск, онбординг и адаптация команды копирайтеров.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading3">
                            <a class="accordion-button collapsed" type="button"
                               data-bs-toggle="collapse"
                               data-bs-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                <div class="container">
                                    <div
                                        class="d-flex flex-lg-row flex-column justify-content-between align-items-lg-center align-items-start content">
                                        <h3>SMM специалист</h3>
                                        <div class="d-flex right align-items-center">
                                            <p class="description">Мы ищем специалиста, который понимает геометрию букв
                                                и
                                                грамотно может написать историю жизни человека.</p>
                                            <svg version="1.1" width="80" height="80" viewBox="0 0 80 80" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="40" cy="40" r="39.5"/>
                                                <path
                                                    d="M37.992 42.264H29.016V38.904H37.992V29.208H41.496V38.904H50.472V42.264H41.496V51.96H37.992V42.264Z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </h2>
                        <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3"
                             data-bs-parent="#accordionVacancy">
                            <div class="accordion-body container">
                                <div class="d-flex flex-lg-row flex-column detailed">
                                    <p class="big-txt">
                                        Что нужно делать
                                    </p>
                                    <ul>
                                        <li>
                                            Контролировать качество работы копирайтеров на проектах (стратегии и
                                            smm-соспровождение), результаты работы группы и KPI.
                                        </li>
                                        <li>
                                            Контролировать и распределять загрузку группы.
                                        </li>
                                        <li>
                                            Контролировать рентабельность группы.
                                        </li>
                                        <li>
                                            Пальпировать настроения и пожелания группы.
                                        </li>
                                        <li>
                                            Работать с мотивацией группы: разрабатывать финансовые и нефинансовые
                                            системы мотиваций.
                                        </li>
                                        <li>
                                            Супервайзить стратегии, участвовать в штурмах, давать своевременную обратную
                                            связь.
                                        </li>
                                        <li>
                                            Составлять планы личного развития команды.
                                        </li>
                                        <li>
                                            Поиск, онбординг и адаптация команды копирайтеров.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="">

                </form>
            </div>
            <div class="container">
                <div class="form-vacancy d-flex flex-lg-row flex-column">
                    <div class="left">
                        <p class="big-txt">Откликнуться <br>
                            на вакансию</p>
                        <p class="small-txt">Вашей вакансии нет в списке?<br>
                            Напишите нам если хотите в команду,<br>
                            возможно для вас найдется вакантное место</p>
                    </div>
                    <div class="right">
                        <form class="ajax-form" id="vacancy_form" action="{{ route('vacancy.send') }}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="blank">
                                <label for="name" class="form-label">Имя</label>
                                <input type="text" name="name" id="name" class="form-control" required>

                                <label for="position" class="form-label">Должность</label>
                                <input type="text" name="position" id="position" class="form-control" required>

                                <label for="phone" class="form-label phone">Контакт для связи</label>
                                <input type="tel" name="phone" id="phone" class="form-control phone" required>

                                <label for="name" class="form-label">Ссылка на соцсети</label>
                                <input type="text" name="social" id="name" class="form-control" required>

                                <label for="description" class="form-label">Расскажите о себе и своем опыте
                                    работы</label>
                                <div class="form-floating">
                                    <textarea class="form-control" placeholder="Leave a comment here" name="description"
                                              id="description" aria-label="default" required></textarea>
                                </div>
                            </div>
                            <div class="resume">
                                <label for="formFile" class="form-label">Резюме</label>
                                <input class="form-control" name="file" type="file" id="formFile" required>
                            </div>
                            <button type="submit" class="submit-button" value="Submit">
                                Отправить отклик
                            </button>
                            <p class="agreement">Нажимая кнопку отправить, вы соглашаетесь <br>
                                на обработку персональных данных.</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
@endsection
