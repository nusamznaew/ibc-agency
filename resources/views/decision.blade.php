@extends('layouts.layout')

@section('title', 'Решение')

@section('cursor', 'black')

@section('page', 'page-white')

@section('extra-css')

@endsection

@section('pixel')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '656243942042560');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
@endsection

@section('content')
    @include('partials.dark-header')
    <div class="page-decision">
        <section class="container">
            <div class="d-flex flex-lg-row flex-column align-items-center header">
                <div class="d-flex flex-column align-self-start left">
                    <h1>60% начинающих бизнесов терпят <br>
                        неудачу из-за ошибок в маркетинге</h1>
                    <p class="desc">Мы поможем избежать их <br>
                        и наполнить ваш бизнес целевыми клиентами</p>
                </div>
                <div class="d-flex justify-content-center align-self-end right">
                    <a href="" class="d-lg-flex d-none magnetic" data-bs-toggle="modal" data-bs-target="#modalRequest" data-magnetic>
                        <div>
                            Оставить <br>
                            заявку
                            <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                 y="0px"
                                 viewBox="0 0 150 150" xml:space="preserve">
                                <g>
                                    <path class="st0" d="M34.66,92.03c8.6,1.16,16.2,4.13,22.65,8.6c6.61,4.63,10.91,10.08,12.9,16.53V13h9.59v104.16
		c1.98-6.45,6.28-11.9,12.73-16.53c6.61-4.46,14.22-7.44,22.82-8.6l2.48,11.08c-22.98,1.32-36.21,15.21-38.03,33.89h-9.59
		c-1.82-18.68-15.05-32.57-38.03-33.89L34.66,92.03z"/>
                                </g>
                            </svg>
                        </div>
                    </a>
                    <a href="" class="d-lg-none d-flex" data-bs-toggle="modal" data-bs-target="#modalRequest">Получить <br>
                        консультацию </a>
                </div>
            </div>
        </section>
        <section class="santa">
            <div class="container d-flex box">
                <div class="left">

                </div>
                <div class="d-flex flex-column align-items-lg-start align-items-center justify-content-center right" data-aos="fade-left" data-aos-duration="1000">
                    <p class="big-txt">
                        Деда мороза не существует
                    </p>
                    <p class="small-txt">
                        Вы построили свой бизнес. И теперь вам нужен грамотный маркетинг, чтобы привлекать клиентов. Но
                        вот в чем загвоздка: вам потребуется много времени и сил, чтобы подобрать хороший штат
                        сотрудников в офис, а также знания и опыт, чтобы контролировать их.
                        <br>
                        <br>
                        Чтобы сэкономить время, деньги и нервы, вам нужно просто нанять готовое подразделение, которое
                        будет маркетингом вашего бизнеса: от разработки стратегии SMM до запуска таргетированной
                        рекламы.
                    </p>
                </div>
            </div>
        </section>
        <section class="paket d-flex flex-column">
            <div class="container d-flex flex-lg-row flex-column justify-content-between align-items-center header2">
                <div class="left" data-aos="fade-right" data-aos-duration="1000">
                    <p class="big-txt">Какое решение мы предлагаем?</p>
                </div>
                <div class="right" data-aos="fade-left" data-aos-duration="1000">
                    <p class="small-txt">У нас есть профессиональная команда маркетологов, которая готова подключиться к
                        продвижению
                        вашего бизнеса. Наши решения уже в первый месяц дадут вам результаты.</p>
                </div>
            </div>
            <div class="d-flex flex-column align-items-lg-center align-items-start paket-uslug" data-aos="zoom-in" data-aos-duration="2000">
                <h2>Пакеты маркетинговых услуг</h2>
                <div class="container">
                    <div class="d-flex justify-content-end paket-header">
                        <div class="lite">LITE</div>
                        <div class="standard">STANDARD</div>
                        <div class="optimal">OPTIMAL</div>
                        <div class="premium">PREMIUM</div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1" style="font-family: 'GraphikLCG-Medium', sans-serif;">Описание услуги</div>
                            <div class="l-2">30 дней</div>
                            <div class="l-3">30 дней</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>30 дней</p>
                                <p>90 дней</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>30 дней</p>
                                <p>90 дней</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Разработка стратегии</div>
                            <div class="l-2">+</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Офрмление хайлайтов и лого</div>
                            <div class="l-2">+</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Статичные картинки в ленту</div>
                            <div class="l-2">-</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Анимированные картинки
                                в ленту</div>
                            <div class="l-2">-</div>
                            <div class="l-3">2</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>4</p>
                                <p>12</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>6</p>
                                <p>18</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Копирайтинг постов</div>
                            <div class="l-2">+</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Индивидуальный
                                контент-план сториз</div>
                            <div class="l-2">-</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Статичные картинки
                                для сториз</div>
                            <div class="l-2">12</div>
                            <div class="l-3">60</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>90</p>
                                <p>270</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>120</p>
                                <p>360</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Видео до 10 сек. для сториз</div>
                            <div class="l-2">-</div>
                            <div class="l-3">-</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Выездная фотосъемка</div>
                            <div class="l-2">-</div>
                            <div class="l-3">-</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>-</p>
                                <p>-</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Перевод сториз на
                                другие языки</div>
                            <div class="l-2">-</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Работа с комментариями,
                                ответ на сообщения в Direct</div>
                            <div class="l-2">-</div>
                            <div class="l-3">-</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Настройка чат-бота в Direct</div>
                            <div class="l-2">-</div>
                            <div class="l-3">-</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>-</p>
                                <p>-</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Разработка УТП (уникального
                                торгового предложения)</div>
                            <div class="l-2">+</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Статичный креатив</div>
                            <div class="l-2">1</div>
                            <div class="l-3">2</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>3</p>
                                <p>9</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>5</p>
                                <p>15</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Анимированный креатив</div>
                            <div class="l-2">-</div>
                            <div class="l-3">1</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>2</p>
                                <p>4</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>3</p>
                                <p>5</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Видео креатив</div>
                            <div class="l-2">-</div>
                            <div class="l-3">-</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>2</p>
                                <p>4</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>3</p>
                                <p>5</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Одностраничный сайт</div>
                            <div class="l-2">-</div>
                            <div class="l-3">+ <br> (Tilda)</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+ <br> (Code)</p>
                                <p>+ <br> (Code)</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+ <br> (Code)</p>
                                <p>+ <br> (Code)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Подключение CRM и
                                обучение</div>
                            <div class="l-2">-</div>
                            <div class="l-3">+ <br> (Tilda CRM)</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+ <br> (AMO CRM)</p>
                                <p>+ <br> (AMO CRM)</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+ <br> (AMO CRM)</p>
                                <p>+ <br> (AMO CRM)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Ведение рекламной
                                кампании</div>
                            <div class="l-2">+</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paket-line">
                    <div class="container h-100">
                        <div class="p-line d-flex">
                            <div class="l-1">Оптимизация рекламной
                                кампании</div>
                            <div class="l-2">+</div>
                            <div class="l-3">+</div>
                            <div class="l-4 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                            <div class="l-5 d-flex justify-content-around">
                                <p>+</p>
                                <p>+</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="d-flex justify-content-end paket-footer">
                        <div class="price" style="font-family: 'GraphikLCG-Medium', sans-serif;">Стоимость</div>
                        <div class="lite">120 000 kzt</div>
                        <div class="standard">210 000 kzt</div>
                        <div class="optimal d-flex justify-content-around">
                            <p>310 000 kzt</p>
                            <p>900 000 kzt</p>
                        </div>
                        <div class="premium d-flex justify-content-around">
                            <p>400 000 kzt</p>
                            <p>1 100 000 kzt</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="express">
            <div class="container d-flex flex-lg-row flex-column justify-content-between">
                <div class="left" data-aos="fade-right" data-aos-duration="1000">
                    Давайте начнем <br> с
                    экспресс стратегии
                </div>
                <div class="right d-flex flex-column align-items-end" data-aos="fade-left" data-aos-duration="1000">
                    <p>
                        Мы лучше познакомимся с вашим бизнесом, пробежимся по основным конкурентам и трендам, после чего сможем подобрать идеальную для вас комбинацию.
                        <br>
                        <br>
                        Сделаем все это за 48 часов.
                        <br>
                        И совершенно бесплатно.
                    </p>
                    <a href="" data-bs-toggle="modal" data-bs-target="#modalRequest">
                        <div>
                            Оставить
                            <br>
                            заявку
                            <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                 y="0px"
                                 viewBox="0 0 150 150" xml:space="preserve">
                                <g>
                                    <path class="st0" d="M34.66,92.03c8.6,1.16,16.2,4.13,22.65,8.6c6.61,4.63,10.91,10.08,12.9,16.53V13h9.59v104.16
		c1.98-6.45,6.28-11.9,12.73-16.53c6.61-4.46,14.22-7.44,22.82-8.6l2.48,11.08c-22.98,1.32-36.21,15.21-38.03,33.89h-9.59
		c-1.82-18.68-15.05-32.57-38.03-33.89L34.66,92.03z"/>
                                </g>
                            </svg>
                        </div>
                    </a>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('extra-js')

@endsection
