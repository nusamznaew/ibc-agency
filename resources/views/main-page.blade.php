@extends('layouts.layout')

@section('title', 'Главная')

@section('cursor', 'white')

@section('page', 'page-dark')

@section('extra-css')

@endsection

@section('pixel')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '656243942042560');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
@endsection

@section('content')
    @include('partials.white-header')
    <div class="container">
        <div class="block-1">
            <div class="d-flex flex-column">
                <h1>Делаем маркетинг, <br>
                    приносящий результат</h1>
                <div class="d-flex flex-column align-self-end right position-relative">
                    <h3>
                        Работаем с вашим проектом только в том случае, <br>
                        если можем заработать для вас минимум в 2 раза больше,
                        чем вы потратите.

                    </h3>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                        <div class="animashka">
                            <div class="kruglyashka">
                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                     y="0px"
                                     viewBox="0 0 150 150" xml:space="preserve">
                                <g>
                                    <path fill="#313131" class="st0" d="M34.66,92.03c8.6,1.16,16.2,4.13,22.65,8.6c6.61,4.63,10.91,10.08,12.9,16.53V13h9.59v104.16
		c1.98-6.45,6.28-11.9,12.73-16.53c6.61-4.46,14.22-7.44,22.82-8.6l2.48,11.08c-22.98,1.32-36.21,15.21-38.03,33.89h-9.59
		c-1.82-18.68-15.05-32.57-38.03-33.89L34.66,92.03z"/>
                                </g>
                            </svg>
                            </div>
                            Бесплатная стратегия продвижения
                        </div>
                    </a>
                    <a href="#cases" class="align-self-end magnetic" data-magnetic>
                        <span class="strelka-vniz">
                            <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                 y="0px"
                                 viewBox="0 0 150 150" xml:space="preserve">
                                <g>
                                    <path fill="#D7D7D7" class="st0" d="M34.66,92.03c8.6,1.16,16.2,4.13,22.65,8.6c6.61,4.63,10.91,10.08,12.9,16.53V13h9.59v104.16
		c1.98-6.45,6.28-11.9,12.73-16.53c6.61-4.46,14.22-7.44,22.82-8.6l2.48,11.08c-22.98,1.32-36.21,15.21-38.03,33.89h-9.59
		c-1.82-18.68-15.05-32.57-38.03-33.89L34.66,92.03z"/>
                                </g>
                            </svg>
                        </span>
                    </a>
                </div>
            </div>
            <div class="line">
            </div>
        </div>
        <div class="block-2" id="cases">
            <div
                class="d-flex flex-lg-row flex-column align-items-lg-stretch align-items-center justify-content-between">
                <a href="/works/podnyali-pribyl-kompanii-v-2-raza-uzhe-v-pervyj-mesyac-vedeniya-targetirovannoj-reklamy-v-instagram" class="d-flex flex-column justify-content-end case1">
                    <video playsinline autoplay muted loop class="gif gif1 position-absolute"
                           src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251421/video1_rrlxel.mp4"></video>
                    <p>Программа "Work <br> and Study USA"</p>
                </a>
                <div class="d-flex flex-column justify-content-between center">
                    <a href="/works/captain-campus" class="d-flex flex-column justify-content-end case2">
                        <video playsinline autoplay muted loop class="gif gif2 position-absolute"
                               src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/video2_aogbm7.mp4"></video>
                        <p>Конкурс на бесплатную подготовку <br> к поступлению в США</p>
                    </a>
                    <a href="/works/pomogli-prodvinut-v-socsetyah-uslugu-socialnoj-adaptacii-v-ssha" class="d-flex flex-column justify-content-end case3">
                        <video playsinline autoplay muted loop class="gif gif3 position-absolute"
                               src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/video3_hzdwej.mp4"></video>
                        <p>Услуги социальной адаптации в США</p>
                    </a>
                </div>
                <div class="d-flex flex-column justify-content-between">
                    <a href="/works/za-schet-targetirovannoj-reklamy-smogli-privlech-klientov-v-shkolu-anglijskogo-yazyka-v-almaty-i-poluchili-neobhodimoe-kolichestvo-uchenikov-na-ves-god" class="d-flex flex-column justify-content-end case4">
                        <img src="{{ asset('video/video4.gif') }}" class="gif gif4 position-absolute" alt="">
                        <p>Языковые курсы</p>
                    </a>
                    <a href="/works/zapustili-onlajn-kurs-sistematizaciya-biznesa-ot-idei-do-realizacii-v-sfere-biznes-obrazovaniya" class="d-flex flex-column justify-content-end case5">
                        <video playsinline autoplay muted loop class="gif gif5 position-absolute"
                               src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251420/video5_rs3ae6.mp4"></video>
                        <p>Образовательный бизнес курс</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="block-3 d-flex flex-lg-row flex-column justify-content-between" data-aos="fade-up">
            <div class="left">
                <p class="big-txt">Направления <br>
                    услуг</p>
                <p class="small-txt">Начав как подразделение внутри группы компаний IBC Group, мы расширили экспертизу и
                    выросли с нашими проектами. Сейчас <br> мы работаем с любыми digital задачами <br> по привлечению
                    клиентов.</p>
            </div>
            <div class="right d-flex flex-column">
                <a href="{{ route('target') }}">Таргетированная реклама</a>
                <a href="#">Social Media Marketing</a>
                <a href="{{ route('web.dev') }}">Разработка веб-сайтов</a>
                <a href="#">Видео производство</a>
            </div>
        </div>
        <div class="block-4">
            <div class="d-flex flex-lg-row flex-column justify-content-between">
                <div class="d-flex flex-column left">
                    <div class="d-flex flex-lg-row flex-column justify-content-between top">
                        <a href="/works/sdelali-dlya-blagotvoritelnogo-fonda-obzornoe-video-uchastiya-sportsmenov-s-ogranichennymi-vozmozhnostyami-v-chempionate-kazahstana-po-bodibildingu" id="paket_1" data-aos="zoom-in-up">
                            <div class="paket-1 d-flex align-items-end">
                                <video playsinline autoplay muted loop
                                       src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/par_b4d47z.mp4"></video>
                                <p>Видео про паралимпийцев</p>
                            </div>
                        </a>
                        <a href="/works/sozdali-s-nulya-kanal-dlya-predprinimatelya-i-sdelali-zapis-pervyh-2-h-intervyu" id="paket_2" data-aos="zoom-in-up">
                            <div class="paket-2 d-flex align-items-end">
                                <video playsinline autoplay muted loop
                                       src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/kyr_barbjz.mp4"></video>
                                <p>Продвижение канала <br> на Youtube</p>
                            </div>
                        </a>
                    </div>
                    <div class="center" data-aos="zoom-in-right">
                        <div class="big-txt">Пакеты решений</div>
                        <div class="small-txt">У нас есть ряд специальных услуг с готовым алгоритмом действия, которые
                            приведут <br> к быстрым результатам. Мы в кратчайший срок адаптируем их под ваши задачи.
                        </div>
                        <a href="{{ route('decision') }}">Подробнее</a>
                    </div>
                </div>
                <div class="d-flex flex-column right">
                    <a href="/works/sozdali-potok-klientov-na-uchastie-v-mezhdunarodnom-forume-v-g-turkestane" id="paket_5" data-aos="zoom-in-up">
                        <div class="paket-5 d-flex align-items-end">
                            <video playsinline autoplay muted loop
                                   src="https://res.cloudinary.com/nusamznaew/video/upload/v1638251419/mezh_eoatvs.mp4"></video>
                            <p>Международный форум <br> «1 000 000$»</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="block-5">
            <div class="row">
                <div class="col-6" data-aos="fade-right" data-aos-duration="1000">
                    <h2>Давайте начнем <br> с экспресс стратегии</h2>
                </div>
                <div class="col-6 position-relative" data-aos="fade-left" data-aos-duration="1000">
                    <p>Мы лучше познакомимся с вашим бизнесом, пробежимся по основным конкурентам и трендам, после чего
                        сможем подобрать идеальную для вас комбинацию.
                        Сделаем все это за 48 часов.
                        <br>
                        И совершенно бесплатно.
                    </p>
                    <div class="position-absolute flag">
                        <a href="" data-bs-toggle="modal" data-bs-target="#modalRequest" class="d-flex magnetic" data-magnetic>
                            <div class="text-center round d-flex align-items-center justify-content-center">
                                <h6 class="mb-0">Оставить <br>заявку
                                    <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                         y="0px"
                                         viewBox="0 0 150 150" xml:space="preserve">
                                <g>
                                    <path fill="#ffffff" class="st0" d="M34.66,92.03c8.6,1.16,16.2,4.13,22.65,8.6c6.61,4.63,10.91,10.08,12.9,16.53V13h9.59v104.16
		c1.98-6.45,6.28-11.9,12.73-16.53c6.61-4.46,14.22-7.44,22.82-8.6l2.48,11.08c-22.98,1.32-36.21,15.21-38.03,33.89h-9.59
		c-1.82-18.68-15.05-32.57-38.03-33.89L34.66,92.03z"/>
                                </g>
                            </svg>
                                </h6>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="line">
        </div>
    </div>
@endsection

@section('extra-js')
    <script type="text/javascript">
        const btnns = document.querySelectorAll(".btnn");

        btnns.forEach((btnn) => {
            btnn.addEventListener("mousemove", function (e) {
                const position = btnn.getBoundingClientRect();
                const x = e.pageX - position.left - position.width / 2;
                const y = e.pageY - position.top - position.height / 2;

                btnn.children[0].style.transform = "translate(" + x * 0.3 + "px, " + y * 0.5 + "px)";
            });
        });

        btnns.forEach((btnn) => {
            btnn.addEventListener("mouseout", function (e) {
                btnn.children[0].style.transform = "translate(0px, 0px)";
            });
        });
    </script>
@endsection
