<!-- Modal -->
<div class="modal fade" id="modalRequest" tabindex="-1" aria-labelledby="modalRequestLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modalka d-flex flex-lg-row flex-column">
                <div class="left d-flex flex-column justify-content-center">
                    <div class="big-txt">Бесплатная консультация</div>
                    <div class="small-txt">Оставьте заявку на бесплатную консультацию и наш специалист свяжется с вами</div>
                </div>
                <div class="right">
                    <form action="{{ route('form.send') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div>
                            <label for="name" class="form-label">Ваше имя</label>
                            <input type="text" name="lead_name" class="form-control" id="lead_name" required>
                        </div>
                        <div>
                            <label for="phone" class="form-label">Ваш телефон</label>
                            <input type="tel" name="lead_phone" class="form-control phone" id="lead_phone" required>
                        </div>
                        <div>
                            <label for="company" class="form-label">Название компании</label>
                            <input type="text" name="lead_company" class="form-control" id="lead_company" style="margin-bottom: 20px" required>
                        </div>
						<div 
							 class="g-recaptcha" 
							 data-sitekey="6LdpoJsdAAAAALHWpKZNa_o3hfl9sX5sn_eU1nmC"
							 data-callback="reCAPTCHA"
							 style="margin-bottom: 20px"
							 ></div>
                        <button id="registerBtn" class="submit-button" type="submit" value="Submit" disabled>
                            Отправить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSuccess" tabindex="-1" aria-labelledby="modalSuccessLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modalka d-flex flex-column align-items-center">
                <div class="big-txt">Спасибо за <br class="d-lg-block d-none"> заявку</div>
                <div class="small-txt">Наш специалист свяжется с вами в <br> рабочее время</div>
                <div class="modal-line"></div>
                <div class="insta">А пока можете посетить наш аккаунт Instagram</div>
                <a href="https://www.instagram.com/ibcagency.kz/"><i class="bi bi-instagram"></i></a>
            </div>
        </div>
    </div>
</div>

