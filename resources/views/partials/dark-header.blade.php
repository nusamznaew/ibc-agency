<div class="container">
    <nav class="navbar navbar-expand-lg nav-dark">
        <div class="container-fluid px-0">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('img/logo-black.png') }}" alt="" width="30" height="24">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="bi bi-list"></i>
            </button>
            <div class="collapse navbar-collapse mt-3" id="navbarSupportedContent">
                <div class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-end w-100">
                    <ul class="navbar-nav mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Главная</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('works.index') }}">Кейсы</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Услуги
                            </a>
                            <ul class="dropdown-menu mt-3 dropdown-menu-dark" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="{{ route('target') }}">Таргетированная реклама</a></li>
                                <li><a class="dropdown-item" href="#">Social Media Marketing</a></li>
                                <li><a class="dropdown-item" href="{{ route('web.dev') }}">Разработка веб-сайтов</a></li>
                                <li><a class="dropdown-item" href="#">Видео производство</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('decision') }}">Решения</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('vacancy') }}">Вакансии</a>
                        </li>
                    </ul>
                    <a href="" class="send-request" data-bs-toggle="modal" data-bs-target="#modalRequest">Оставить заявку</a>
                </div>
            </div>
        </div>
    </nav>
</div>
