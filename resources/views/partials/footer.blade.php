<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 d-flex flex-column order-lg-1 order-3">
                <a href="/" class="logo order-lg-1 order-3">
                    <img src="{{ asset('img/logo-white.png') }}" alt="logo">
                </a>
                <div class="d-flex social order-lg-2 order-1">
                    <a href="">
                        <i class="bi bi-instagram"></i>
                    </a>
                    <a href="">
                        <i class="bi bi-facebook"></i>
                    </a>
                    <a href="">
                        <i class="bi bi-telegram"></i>
                    </a>
                </div>
                <div class="order-lg-3 order-2">
                    <p class="d-lg-none d-block">Политика конфиденциальности</p>
                    <p class="mb-lg-0 mb-4">COPYRIGHT © 2021 IBC AGENCY</p>
                </div>
            </div>
            <div class="col-lg-4 d-flex flex-column uslugi order-lg-2 order-2">
                <p class="big-txt">Услуги:</p>
                <a href="{{ route('target') }}">Таргетированная реклама</a>
                <a href="">Social Media Marketing</a>
                <a href="{{ route('web.dev') }}">Разработка веб-сайтов</a>
                <a href="">Видео производство</a>
                <a href="{{ route('decision') }}" class="last">Пакетные решения</a>
                <a href="{{ asset('presentation.pdf') }}" class="presentation" download style="margin-bottom: 0">Презентация</a>
            </div>
            <div class="col-lg-4 d-flex flex-column contact order-lg-3 order-1">
                <p class="big-txt">Связаться:</p>
                <a href="tel:+7 775 094-74-17" class="num">+7 775 094-74-17</a>
                <a href="" class="order-callback" data-bs-toggle="modal" data-bs-target="#modalRequest">Заказать обратный звонок</a>
                <p class="big-txt">Где нас найти:</p>
                <p>г.Алматы, ул.Хаджимукана 49</p>
                <p class="mt-auto d-lg-block d-none">Политика конфиденциальности</p>
            </div>
        </div>
    </div>
</footer>
