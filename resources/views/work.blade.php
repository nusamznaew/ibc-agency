@extends('layouts.layout')

@section('title', $work->title)

@section('cursor', 'black')

@section('page', 'page-white')

@section('extra-css')

@endsection

@section('pixel')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '656243942042560');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
@endsection

@section('content')
    <div class="page-case">
        <div class="header" style="background-image: linear-gradient(0deg, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('{{ asset('storage/'. $work->background) }}');">
            @include('partials.white-header')
            <div class="container">
                <h1>{!! $work->title !!}</h1>
            </div>
        </div>
        <div class="case-content">
            <div class="d-flex flex-column container">
                <div class="text-1"  data-aos="fade-right" data-aos-duration="500">
                    Цель и задачи <br> кампании
                </div>
                <div class="d-flex flex-lg-row flex-column justify-content-between top">
                    <div class="d-flex flex-column left" data-aos="fade-right" data-aos-duration="500">
                        <div class="text-2">Цель:</div>
                        <p>
                            {!! $work->purpose !!}
                        </p>
                    </div>
                    <div class="d-flex flex-column right" data-aos="fade-left" data-aos-duration="500">
                        <div class="text-2">Ключевая проблема:</div>
                        <p>
                            {!! $work->key_problem !!}
                        </p>
                    </div>
                </div>
                <div class="d-flex flex-lg-row flex-column center">
                    <div class="text-1 left" data-aos="fade-right" data-aos-duration="500">Выбранная стратегия</div>
                    <div class="right" data-aos="fade-left" data-aos-duration="500">
                        {!! $work->strategy !!}
                    </div>
                </div>
                <div class="d-flex flex-lg-row flex-column bottom">
                    <div class="d-flex flex-column left">
                        <p>Бюджет</p>
                        <div class="text-2" data-aos="fade-right" data-aos-duration="500">{!! trim(strrev(chunk_split(strrev($work->budget),3, ' '))) !!} $</div>
                        <p>Целевых заявок</p>
                        <div class="text-2" data-aos="fade-right" data-aos-duration="500">{!! $work->targeted_application !!}</div>
                    </div>
                    <div class="d-flex flex-column right">
                        <p>Цена лида:</p>
                        <div class="text-2" data-aos="fade-right" data-aos-duration="500">{!! $work->lead_price !!} $</div>
                        <p>Общий охват компании</p>
                        <div class="text-2" data-aos="fade-right" data-aos-duration="500">{!! trim(strrev(chunk_split(strrev($work->company_coverage),3, ' '))) !!}</div>
                    </div>
                </div>
            </div>
            <div class="container d-flex flex-column media">
                <div class="text-1">
                    Материалы созданные для <br> рекламной кампании:
                </div>
				@if(!empty($work->stories))
					<p>Креативы для stories:</p>
					<div class="d-flex flex-wrap story">
						@foreach(json_decode($work->stories, true) as $stories)
							<img src="{{ Voyager::image($stories) }}" data-aos="flip-left" data-aos-duration="500"/>
						@endforeach
					</div>
				@endif
				@if(!empty($work->posts))
					<p>Креативы для постов:</p>
					<div class="d-flex flex-wrap post">
						@foreach(json_decode($work->posts, true) as $posts)
							<img src="{{ Voyager::image($posts) }}" data-aos="flip-left" data-aos-duration="500"/>
						@endforeach
					</div>
				@endif
                <p>Видео-креативы для рекламной кампании:</p>
                <div class="d-flex flex-wrap video">
                    {!! $work->video !!}
                </div>
            </div>
        </div>
        <div class="form">
            <div class="container d-flex flex-lg-row flex-column justify-content-between">
                <div class="d-flex flex-column left" data-aos="fade-right" data-aos-duration="500">
                    <div class="text-1">
                        Оставьте заявку для получения бесплатной мини-стратегии, расчета трафика и стоимости <br> заявок
                        в вашей нише
                    </div>
                    <div>
                        Интегрируемся в ваш бизнес, структурируем <br> маркетинг и повысим продажи:
                    </div>
                    <ul>
                        <li>
                            Соберем гибкую команду под ваш проект, нацеленную на решение задачи;
                        </li>
                        <li>
                            Установим понятные KPI;
                        </li>
                        <li>
                            Проанализируем рынок, продукт, конкурентов <br> и вашу целевую аудиторию;
                        </li>
                        <li>
                            Разработаем взаимосвязанную рабочую систему.
                        </li>
                    </ul>
                    <div>

                    </div>
                </div>
                <div class="d-flex flex-column right" data-aos="fade-left" data-aos-duration="500">
                    <div class="d-flex flex-column">
                        <form action="{{ route('works.send') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <label for="name" class="form-label">Имя</label>
                            <input type="text" name="work_name" class="form-control" id="name">
                            <label for="phone" class="form-label">Ваш телефон</label>
                            <input type="tel" name="work_phone" class="form-control phone" id="phone">
                            <label for="phone" class="form-label">Описание</label>
                            <div class="form-floating">
                                <textarea class="form-control" name="work_description" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                <label for="floatingTextarea"></label>
                            </div>
                            <button type="submit">Отправить</button>
                        </form>
                        <small>Нажимая кнопку отправить, вы соглашаетесь <br>
                            на обработку персональных данных.</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="more-cases">
            <div class="container">
                <div class="big-txt">
                    Больше кейсов
                </div>
                <div class="d-flex flex-lg-row flex-column justify-content-between">
                    <div class="boxiki" data-aos="zoom-in-right" data-aos-duration="1000"></div>
                    <div class="boxiki" data-aos="zoom-in-up" data-aos-duration="1000"></div>
                    <div class="boxiki" data-aos="zoom-in-left" data-aos-duration="1000"></div>
                </div>
                <div class="line">

                </div>
            </div>
        </div>
        <div class="kara-shapan"></div>
    </div>
@endsection
