@extends('layouts.layout')

@section('title', 'Разработка веб-сайтов')

@section('cursor', 'black')

@section('page', 'page-white')

@section('extra-css')

@endsection

@section('pixel')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '656243942042560');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
@endsection

@section('content')
    @include('partials.dark-header')
    <div class="page-web">
        <section class="container header d-flex flex-lg-row flex-column justify-content-between">
            <div class="left">
                <p class="big-txt">Создаем сайты, которые решают задачи бизнеса</p>
                <p class="small-txt">Разрабатываем сайт, который будет привлекать новых клиентов, выделять на рынке.
                    Всего за 2 недели.</p>
            </div>
            <div class="right align-self-lg-end align-self-center">
                <a href="" data-bs-toggle="modal" data-bs-target="#modalRequest" class="d-flex magnetic" data-magnetic>
                    <div>
                        Получить <br>
                        консультацию
                    </div>
                </a>
            </div>
        </section>
        <section class="plan d-flex flex-column align-items-center">
            <div class="container">
                <div class="d-flex flex-column">
                    <p class="head-txt" data-aos="fade-left" data-aos-duration="1000">Какой сайт мне нужен?</p>
                    <div class="d-flex flex-column stairs">
                        <div class="d-flex justify-content-start" data-aos="fade-left" data-aos-duration="1000">
                            <div class="d-flex box align-items-center">
                                <img src="{{ asset('img/website.png') }}" alt="website">
                                <div class="d-flex flex-column txt-box">
                                    <div class="big-txt">
                                        Лендинг
                                    </div>
                                    <div class="small-txt">
                                        Одностраничный сайт для сбора заявок. Нужен, когда вы запускаете новый продукт
                                        или проводите презентацию компании.
                                        Подойдет, когда в ассортименте
                                        1-3 услуги или продукта
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center" data-aos="fade-left" data-aos-duration="1000">
                            <div class="d-flex box align-items-center">
                                <img src="{{ asset('img/corp.png') }}" alt="website">
                                <div class="d-flex flex-column txt-box">
                                    <div class="big-txt">
                                        Корпоративный сайт
                                    </div>
                                    <div class="small-txt">
                                        Многостраничный сайт. Нужен для взаимодействия аудитории с брендом. Его цель -
                                        максимально проинформировать своего клиента о вашей компании
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end" data-aos="fade-left" data-aos-duration="1000">
                            <div class="d-flex box align-items-center">
                                <img src="{{ asset('img/shop.png') }}" alt="website">
                                <div class="d-flex flex-column txt-box">
                                    <div class="big-txt">Интернет-магазин</div>
                                    <div class="small-txt">
                                        Многостраничный сайт с системой оплаты. Подойдет вам, если у
                                        вас есть ассортимент продуктов. Вы сможете автоматически принимать оплаты за
                                        заказы и оформлять доставку
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-column position-relative how-we-work">
                    <div class="head-box d-flex flex-column">
                        <h2 class="text-center" data-aos="fade-down" data-aos-duration="1000">Как мы работаем</h2>
                    </div>
                    <div class="list d-flex flex-lg-row flex-column align-items-lg-start align-items-center" data-aos="zoom-out" data-aos-duration="1000">
                        <p class="big-txt">
                            Проводим <br class="d-lg-none d-block"> исследование
                            <br>
                            <br>
                            <span>01</span>
                        </p>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Компании
                            </p>
                            <p class="txt">Изучаем как работает бизнес, вникаем в продукт, выделяем сильные и слабые
                                стороны. Подбираем подходящий формат сайта</p>
                        </div>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Аудитории
                            </p>
                            <p class="txt">Находим интересы, боли. Смотрим их поведение в интернете и жизни. Только так
                                мы сможем понять истинные мотивы покупки</p>
                        </div>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Конкурентов
                            </p>
                            <p class="txt">Изучаем, что предлагают и чем выделяются. Находим лучшие решения и
                                практики</p>
                        </div>
                    </div>
                    <div class="box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                        <h2 class="text-center">Мы узнаем ваш бизнес и понимаем, в какую сторону <br> двигаться при
                            создании прототипа и дизайна.</h2>
                    </div>
                    <div class="list d-flex flex-lg-row flex-column align-items-lg-start align-items-center" data-aos="zoom-out" data-aos-duration="1000">
                        <p class="big-txt">
                            Проектируем <br>
                            прототип
                            <br>
                            <br>
                            <span>02</span>
                        </p>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Структура
                            </p>
                            <p class="txt">Прописываем структуру сайта, которая будет вести вашего клиента за руку,
                                знакомить с вашим продуктом или услугой и сподвигнет совершить нужное вам действие</p>
                        </div>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Текст
                            </p>
                            <p class="txt">Исходя из исследования, пишем текст, который зацепит вашу аудиторию, передаст
                                нужные чувства и смотивирует на активное действие (купить или сделать звонок)</p>
                        </div>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Прототип
                            </p>
                            <p class="txt">Создаем прототип вашего сайта в виде блоков с текстами и графическими
                                формами</p>
                        </div>
                    </div>
                    <div class="box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                        <h2 class="text-center">На этом этапе вы сможете прямо в прототипе внести <br> необходимые
                            правки и добавить пожелания.</h2>
                    </div>
                    <div class="list d-flex flex-lg-row flex-column align-items-lg-start align-items-center" data-aos="zoom-out" data-aos-duration="1000">
                        <p class="big-txt">
                            Создаем <br>
                            дизайн
                            <br>
                            <br>
                            <span>03</span>
                        </p>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Вдохновение
                            </p>
                            <p class="txt">Собираем идеи и решения для вашего сайта у лидеров вашей сферы деятельности
                                по всему миру</p>
                        </div>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Концепция
                            </p>
                            <p class="txt">Подбираем подходящие цвета, выбираем шрифты, общий стиль и идею сайта.
                                Согласовываем стиль</p>
                        </div>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Визуальное воплощение
                            </p>
                            <p class="txt">Создаем уникальный и запоминающийся сайт, который посетитель точно оценит и
                                запомнит</p>
                        </div>
                    </div>
                    <div class="box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                        <h2 class="text-center">На этом этапе полностью готов и согласован макет <br> дизайна сайта.
                            Вносятся финальные правки.</h2>
                    </div>
                    <div class="list d-flex flex-lg-row flex-column align-items-lg-start align-items-center" data-aos="zoom-out" data-aos-duration="1000">
                        <p class="big-txt">
                            Верстаем и <br>
                            настраиваем
                            <br>
                            <br>
                            <span>04</span>
                        </p>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Верстка
                            </p>
                            <p class="txt">Делаем так, чтобы сайт загружался быстро на всех устройствах и во всех
                                браузерах</p>
                        </div>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Интеграция
                            </p>
                            <p class="txt">Подключаем метрики, систему оплаты, удобную корзину, при необходимости.
                                Интегрируем CRM системы</p>
                        </div>
                        <div class="d-flex flex-column boxiha flex-column">
                            <p class="main-txt">
                                <img src="{{ asset('img/circle.png') }}" alt="">
                                Тестирование
                            </p>
                            <p class="txt">Проверяем работоспособность всех функций сайта на разных устройствах</p>
                        </div>
                    </div>
                    <div class="box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                        <h2 class="text-center">Сайт готов, можно смело запускать <br> трафик и получать первых клиентов
                        </h2>
                    </div>
                    <div class="head-box d-flex flex-column mt-5" data-aos="zoom-in" data-aos-duration="1000">
                        <h2 class="text-center">Как мы работаем</h2>
                    </div>
                    <div class="d-lg-flex d-none pizza justify-content-center">
                        <div class="left d-flex flex-column align-items-center justify-content-between">
                            <div data-aos="fade-down-right" data-aos-duration="1000">Вы получаете эффективный инструмент для продвижения и продаж ваших услуг</div>
                            <div data-aos="fade-up-right" data-aos-duration="1000">Заявки на автопилоте. Продвигаете специальные предложения и генерируете клиентов с
                                помощью маркетинговых воронок
                            </div>
                        </div>
                        <div class="center d-flex justify-content-center" data-aos="zoom-in" data-aos-duration="1000">
                            <img class="big-circle" src="{{ asset('img/big-circle.png') }}" alt="">
                        </div>
                        <div class="right d-flex flex-column align-items-center justify-content-between">
                            <div data-aos="fade-down-left" data-aos-duration="1000">Вас узнает аудитория и проявляет больше доверия</div>
                            <div data-aos="fade-up-left" data-aos-duration="1000">Эффективнее, чем презентация. Понятное описание всех сильных сторон продукта в удобном
                                формате
                            </div>
                        </div>
                    </div>
                    <img class="d-lg-none d-block" src="{{ asset('img/small-circle.png') }}" alt="">
                </div>
            </div>
            <div class="saplya d-flex flex-column align-items-center position-relative" data-aos="flip-down" data-aos-duration="1000">
                <h3>Получите поэтапный план работ <br class="d-lg-block d-none"> со сроками по вашему проекту</h3>
                <div class="d-flex flex-lg-row flex-column justify-content-between steps">
                    <div class="d-flex flex-column">
                        <div class="d-flex align-items-center">
                            <p class="big-txt">1</p>
                            <p class="small-txt">Заполнение брифа <br>
                                (1 день)</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">2</p>
                            <p class="small-txt">Анализ решений <br>
                                имеющихся на <br>
                                рынке (2 дня)</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">3</p>
                            <p class="small-txt">Cбор рефернсов <br>
                                по схожим <br>
                                тематикам и кросс <br>
                                тематикам (2 дня)</p>
                        </div>
                    </div>
                    <div class="d-flex flex-column">
                        <div class="d-flex align-items-center">
                            <p class="big-txt">4</p>
                            <p class="small-txt">Формирование прототипа <br>
                                сайта/лендинга (4 дня)</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">5</p>
                            <p class="small-txt">Дизайн часть (UI) <br>
                                (4 дня)</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">6</p>
                            <p class="small-txt">Верстка на платформе <br>
                                (3 дня)</p>
                        </div>
                    </div>
                    <div class="d-flex flex-column">
                        <div class="d-flex">
                            <p class="big-txt">7</p>
                            <p class="small-txt">Согласование</p>
                        </div>
                        <div class="d-flex">
                            <p class="big-txt">8</p>
                            <p class="small-txt">Подключение форм <br> приёма заявок + <br> аналитики (1 день)</p>
                        </div>
                        <div class="d-flex">
                            <p class="big-txt">
                                <svg width="28" height="28" viewBox="0 0 28 28" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M14 0L17.6431 5.20467L23.8995 4.1005L22.7953 10.3569L28 14L22.7953 17.6431L23.8995 23.8995L17.6431 22.7953L14 28L10.3569 22.7953L4.1005 23.8995L5.20467 17.6431L0 14L5.20467 10.3569L4.1005 4.1005L10.3569 5.20467L14 0Z"
                                        fill="#1C54FC"/>
                                </svg>
                            </p>
                            <p class="small-txt">Сайт полностью готов <br>
                                к рекламе</p>
                        </div>
                    </div>
                </div>
                <a href="" class="take-plan" data-bs-toggle="modal" data-bs-target="#modalRequest">
                    <div>
                        Получить
                        <br>
                        план
                    </div>
                </a>
            </div>
        </section>
        <section class="price-uslug position-relative">
            <div class="container">
                <h3 data-aos="fade-right" data-aos-duration="1000">Стоимость услуг</h3>
                <div class="d-flex flex-wrap align-items-start justify-content-between uslugi">
                    <div class="d-flex flex-column box" data-aos="fade-down-right" data-aos-duration="1000">
                        <h4>Консультация</h4>
                        <p class="price">От 20 000 тенге</p>
                        <p>Поговорим о вашем проекте, обсудим самые разные задачи и найдем решение. В процессе вы
                            получите экспресс-анализ вашего бизнеса, ответы на вопросы, а также план действий на
                            будущее.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Оставить заявку
                            </div>
                        </a>
                    </div>
                    <div class="d-flex flex-column box" data-aos="fade-down-left" data-aos-duration="1000">
                        <h4>Лендинг</h4>
                        <p class="price">От 80 000 тенге</p>
                        <p>Создание дизайна и контента страницы, которая призывает пользователя к действию.
                            Например, подписаться на рассылку, купить билет на конференцию, запросить смету
                            или просто скачать презентацию.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Оставить заявку
                            </div>
                        </a>
                    </div>
                    <div class="d-flex flex-column box" data-aos="fade-up-right" data-aos-duration="1000">
                        <h4>Многостраничник</h4>
                        <p class="price">От 250 000 тенге</p>
                        <p>Создание сайта с более сложной структурой: несколькими страницами, которые объединены меню
                            или другими блоками с ссылками.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Оставить заявку
                            </div>
                        </a>
                    </div>
                    <div class="d-flex flex-column box" data-aos="fade-up-left" data-aos-duration="1000">
                        <h4>Интернет-магазин</h4>
                        <p class="price">От 500 000 тенге</p>
                        <p>Создание сайта для продажи товаров с платежной системой, корзиной и уведомлением продавца и
                            покупателя о заказе.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Оставить заявку
                            </div>
                        </a>
                    </div>
                </div>
                <div class="free d-flex flex-lg-row flex-column" data-aos="fade-up" data-aos-duration="1000">
                    <div class="left">
                        <div class="big-txt">Можно бесплатно?</div>
                        <div class="small-txt">
                            Да можно. Подробно опишите свой проект, сформулируйте вопросы. Поделитесь ссылкой на наш
                            сайт и мы подарим вам консультацию.
                        </div>
                    </div>
                    <div class="right d-flex align-items-center justify-content-center">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Рассказать
                            </div>
                        </a>
                    </div>
                </div>
                <h3 data-aos="fade-right" data-aos-duration="1000">Актуальные кейсы:</h3>
                <div class="d-flex flex-lg-row flex-column justify-content-between">
                    <div class="boxiki" data-aos="zoom-in-right" data-aos-duration="1000"></div>
                    <div class="boxiki" data-aos="zoom-in-up" data-aos-duration="1000"></div>
                    <div class="boxiki" data-aos="zoom-in-left" data-aos-duration="1000"></div>
                </div>
                <div class="line">

                </div>
            </div>
            <div class="kara-shapan"></div>
        </section>
    </div>
@endsection
