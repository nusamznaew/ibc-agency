@extends('layouts.layout')

@section('title', 'Карьера')

@section('cursor', 'black')

@section('page', 'page-white')

@section('extra-css')

@endsection

@section('pixel')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '656243942042560');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
@endsection

@section('content')
    @include('partials.dark-header')
    <div class="page-target">
        <section class="container header d-flex flex-lg-row flex-column justify-content-between">
            <div class="left">
                <p class="big-txt" data-cursor="-opaque">Наладим поток продаж <br class="d-lg-none d-block"> с <br class="d-lg-block d-none">
                    помощью таргета в соц.сетях</p>
                <p class="small-txt">Создаем рекламу, заточенную под ваши цели и бизнес-модель. Гарантируем рост продаж
                    на 30% и результаты уже в первые дни.</p>
            </div>
            <div class="right align-self-lg-end align-self-center">
                <a href="" data-bs-toggle="modal" data-bs-target="#modalRequest" class="d-flex magnetic" data-magnetic>
                    <div>
                        Получить <br>
                        консультацию
                    </div>
                </a>
            </div>
        </section>
        <section class="plan d-flex flex-column align-items-center">
            <div class="container d-flex flex-column position-relative">
                <div class="head-box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                    <h2 class="text-center">Гениальное - просто. <br class="d-lg-none d-block"> Мы меняем <br
                            class="d-lg-block d-none">
                        подход к продвижению проектов:</h2>
                </div>
                <div class="list d-flex flex-lg-row flex-column align-items-lg-start align-items-center" data-aos="zoom-out" data-aos-duration="1000">
                    <p class="big-txt">
                        Создаем <br class="d-lg-none d-block"> стратегию
                        <br>
                        <br>
                        <span>01</span>
                    </p>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Проводим исследование
                        </p>
                        <p class="txt">Собираем воедино все данные, связанные с вашей задачей. Как правило это данные о
                            рынке (конкуренты + спрос + предложения).</p>
                    </div>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Разбиваем аудиторию на сегменты
                        </p>
                        <p class="txt">Разделяем аудиторию по общим интересам, геоположению, потребностям и тд.</p>
                    </div>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Выстраиваем последовательность коммуникации
                        </p>
                        <p class="txt">Структурируем кампании по промежуткам, в рамках которых тестируем гипотезы.</p>
                    </div>
                </div>
                <div class="box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                    <h2 class="text-center">В руках готовый план продвижения. <br> Осталось только реализовать.</h2>
                </div>
                <div class="list d-flex flex-lg-row flex-column align-items-lg-start align-items-center" data-aos="zoom-out" data-aos-duration="1000">
                    <p class="big-txt">
                        Реализуем <br class="d-lg-none d-block">
                        стратегию <br class="d-lg-none d-block">
                        на деле
                        <br>
                        <br>
                        <span>02</span>
                    </p>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Создаем контент
                        </p>
                        <p class="txt">Продумываем УТП, делаем дизайн для рекламной кампании.</p>
                    </div>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Создаем стиль
                        </p>
                        <p class="txt">Создаем единый образ, который используется во всех рекламных кампаниях.</p>
                    </div>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Запускаем
                        </p>
                        <p class="txt">Запускаем показ объявлений по выявленной аудитории без банов от фейсбук.</p>
                    </div>
                </div>
                <div class="box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                    <h2 class="text-center">Реклама заработала. Заявки идут. Дальше осталось <br> только оптимизировать
                        и масштабировать.</h2>
                </div>
                <div class="list d-flex flex-lg-row flex-column align-items-lg-start align-items-center" data-aos="zoom-out" data-aos-duration="1000">
                    <p class="big-txt">
                        Оптимизируем <br class="d-lg-none d-block">
                        рекламную <br class="d-lg-none d-block">
                        кампанию
                        <br>
                        <br>
                        <span>03</span>
                    </p>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Проводим тесты
                        </p>
                        <p class="txt">Новые сегменты аудитории, интересы, гео и тд.</p>
                    </div>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Создаем планы
                        </p>
                        <p class="txt">Собираем все данные о действиях в одну карту и анализируем метрики (ROMI, CAC,
                            CTR, CPA и тд).</p>
                    </div>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Маркетинговый подход
                        </p>
                        <p class="txt">Для повышения эффективности готовим рекомендации по внедрению CRM систем.</p>
                    </div>
                </div>
                <div class="box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                    <h2 class="text-center">Реклама идет еще лучше. Получаем <br> контролируемый поток заявок и план
                        роста.</h2>
                </div>
                <div class="list d-flex flex-lg-row flex-column align-items-lg-start align-items-center" data-aos="zoom-out" data-aos-duration="1000">
                    <p class="big-txt">
                        Масштабирование
                        <br>
                        <br>
                        <span>04</span>
                    </p>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Комплексный подход
                        </p>
                        <p class="txt">Выявляем лучшие связки и масштабируем их, сохраняя при этом положительный
                            ROAS</p>
                    </div>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Определяем KPI
                        </p>
                        <p class="txt">Мы фиксируем KPI после теста и растем строго в этих рамках</p>
                    </div>
                    <div class="d-flex flex-column boxiha flex-column">
                        <p class="main-txt">
                            <img src="{{ asset('img/circle.png') }}" alt="">
                            Создаем инфраструктуру
                        </p>
                        <p class="txt">Фиксируем и отрабатываем каждый шаг, четко структурируя маркетинг</p>
                    </div>
                </div>
                <div class="box d-flex flex-column" data-aos="fade-down" data-aos-duration="1000">
                    <h2 class="text-center">Выключены неработающие связки. На основании результатов <br>
                        настроены эффективные рекламные кампании, которые дают <br>
                        больше клиентов при тех же первоначальных расходов.</h2>
                </div>
            </div>
            <div class="saplya d-flex flex-column align-items-center position-relative" data-aos="flip-down" data-aos-duration="1000">
                <h3>Получите поэтапный план работ <br class="d-lg-block d-none"> со сроками по вашему проекту</h3>
                <div class="d-flex flex-lg-row flex-column justify-content-between steps">
                    <div class="d-flex flex-column">
                        <div class="d-flex align-items-center">
                            <p class="big-txt">1</p>
                            <p class="small-txt">Заполнение брифа</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">2</p>
                            <p class="small-txt">Изучение вашей <br>
                                воронки продаж</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">3</p>
                            <p class="small-txt">Изучение <br>
                                вашего проекта: <br>
                                офферы, УТП, <br>
                                уникальность</p>
                        </div>
                    </div>
                    <div class="d-flex flex-column">
                        <div class="d-flex align-items-center">
                            <p class="big-txt">4</p>
                            <p class="small-txt">Утверждение плана по <br>
                                количеству заявок</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">5</p>
                            <p class="small-txt">Разработка стратегии <br>
                                рекламной кампании</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">6</p>
                            <p class="small-txt">Копирайтер пишет <br>
                                тексты, смыслы, офферы</p>
                        </div>
                    </div>
                    <div class="d-flex flex-column">
                        <div class="d-flex">
                            <p class="big-txt">7</p>
                            <p class="small-txt">Дизайнеры делают <br>
                                графические креативы</p>
                        </div>
                        <div class="d-flex">
                            <p class="big-txt">8</p>
                            <p class="small-txt">Согласование всех <br>
                                материалов. Запуск.</p>
                        </div>
                        <div class="d-flex">
                            <p class="big-txt">
                                <svg width="28" height="28" viewBox="0 0 28 28" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M14 0L17.6431 5.20467L23.8995 4.1005L22.7953 10.3569L28 14L22.7953 17.6431L23.8995 23.8995L17.6431 22.7953L14 28L10.3569 22.7953L4.1005 23.8995L5.20467 17.6431L0 14L5.20467 10.3569L4.1005 4.1005L10.3569 5.20467L14 0Z"
                                        fill="#1C54FC"/>
                                </svg>
                            </p>
                            <p class="small-txt">А/В тестирование, <br>
                                поддержка и <br>
                                оптимизация</p>
                        </div>
                    </div>
                </div>
                <a href="" class="take-plan" data-bs-toggle="modal" data-bs-target="#modalRequest">
                    <div>
                        Получить
                        <br>
                        план
                    </div>
                </a>
            </div>
        </section>
        <section class="price-uslug position-relative">
            <div class="container">
                <h3 data-aos="fade-right" data-aos-duration="1000">Стоимость услуг</h3>
                <div class="d-flex flex-lg-row flex-column align-items-start justify-content-between uslugi">
                    <div class="d-flex flex-column box" data-aos="zoom-in-up" data-aos-duration="1000">
                        <h4>Настройка рекламных кампаний</h4>
                        <p class="price">От 80 000 тенге</p>
                        <p>Возьмем на себя все вопросы по разработке рекламной кампании. Исследуем. Запустим.
                            Оптимизируем.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Оставить заявку
                            </div>
                        </a>
                    </div>
                    <div class="d-flex flex-column box" data-aos="zoom-in-up" data-aos-duration="1000">
                        <h4>Можно бесплатно?</h4>
                        <p>Да, можно бесплатно получить у нас консультацию: поговорим о вашем проекте, обсудим самые
                            разные задачи и найдем решения. В процессе вы получите экспресс-анализ вашего маркетинга,
                            ответы на вопросы, а также план действий</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Записаться на консультацию
                            </div>
                        </a>
                    </div>
                </div>
                <h3>Актуальные кейсы:</h3>
                <div class="d-flex flex-lg-row flex-column justify-content-between">
                    <div class="boxiki" data-aos="zoom-in-right" data-aos-duration="1000"></div>
                    <div class="boxiki" data-aos="zoom-in-up" data-aos-duration="1000"></div>
                    <div class="boxiki" data-aos="zoom-in-left" data-aos-duration="1000"></div>
                </div>
                <div class="line">

                </div>
            </div>
            <div class="kara-shapan"></div>
        </section>
    </div>
@endsection
