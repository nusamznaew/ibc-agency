@extends('layouts.layout')

@section('title', 'Карьера')

@section('page', 'page-dark')

@section('extra-css')

@endsection

@section('pixel')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '656243942042560');
	  fbq('track', 'PageView');
	  fbq('track', 'Lead');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
@endsection

@section('content')
    @include('partials.white-header')
    <div class="page_success d-flex flex-column align-items-center">
        <div class="big-txt">Спасибо за <br class="d-lg-block d-none"> заявку</div>
        <div class="small-txt">Наш специалист свяжется с вами в <br> рабочее время</div>
        <div class="modal-line"></div>
        <div class="insta">А пока можете посетить наш аккаунт Instagram</div>
        <a href="https://www.instagram.com/ibcagency.kz/"><i class="bi bi-instagram"></i></a>
    </div>
@endsection
