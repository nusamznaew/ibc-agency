@extends('layouts.layout')

@section('title', 'Продвижение бизнеса в социальных сетях')

@section('cursor', 'black')

@section('page', 'page-white')

@section('extra-css')

@endsection

@section('pixel')
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '656243942042560');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=656243942042560&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
@endsection

@section('content')
    @include('partials.dark-header')
    <div class="page-marketing">
        <section class="container header d-flex flex-lg-row flex-column justify-content-between">
            <div class="left d-flex flex-column">
                <p class="big-txt" data-cursor="-opaque">Продвижение бизнеса в <br> социальных сетях</p>
                <div class="d-flex flex-column heduha">
                    <div class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column">
                            <div class="d-flex flex-column">
                                <div class="big">ФИКСИРУЕМ</div>
                                <div class="small">стоимость заявки в <br> договоре</div>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="big">ОТ 5 СПЕЦИАЛИСТОВ</div>
                                <div class="small">в команде по каждому <br> проекту</div>
                            </div>
                        </div>
                        <div class="d-flex flex-column">
                            <div class="d-flex flex-column">
                                <div class="big">ФИНАНСОВАЯ</div>
                                <div class="small">ответственность за <br> проекты</div>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="big">>300</div>
                                <div class="small">прибыльных <br>
                                    кейсов
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right align-self-lg-end align-self-center">
                <a href="" data-bs-toggle="modal" data-bs-target="#modalRequest" class="d-flex magnetic" data-magnetic>
                    <div>
                        Получить <br>
                        консультацию
                    </div>
                </a>
            </div>
        </section>
        <section class="info d-flex flex-column align-items-center">
            <div class="container d-flex flex-column">
                <div class="head-title"><span>9 КОНКРЕТНЫХ ШАГОВ</span> ПО ПРОДВИЖЕНИЮ <br> ВАШЕГО БИЗНЕСА В ПЕРВОМ
                    МЕСЯЦЕ
                </div>
            </div>
            <div class="container steps d-flex flex-wrap justify-content-between">
                <div class="step">
                    <div class=" d-flex flex-column">
                        <div class="hitman">
                            <object data="/img/search.svg" type=""></object>
                            <div class="text">Оценим конкурентную среду на вашем рынке</div>
                        </div>
                        <div class="text2">
                            Узнаем все плюсы и минусы ваших конкурентов, а также найдем с помощью специализированных
                            инструментов и сервисов подбедную стратегию присутствия в соц.сетях
                        </div>
                    </div>
                </div>
                <div class="step d-flex flex-column">
                    <div class="hitman">
                        <object data="/img/doc.svg" type=""></object>
                        <div class="text">Создадим продающий рубрикатор</div>
                    </div>
                    <div class="text2">
                        Продажа в соц.сетях является точно таким же движенем клиента по воронке продаж, как и в любом
                        другом источнике. Именно поэтому мы подготавливаем рубрикатор-воронку для успешных продаж в
                        соц.сетях
                    </div>
                </div>
                <div class="step d-flex flex-column">
                    <div class="hitman">
                        <object data="/img/design.svg" type=""></object>
                        <div class="text">Подготовим индивидуальный дизайн</div>
                    </div>
                    <div class="text2">
                        Индивидуальный дизайн - ваша визитная карточка и залог узнаваемости бренда. Поэтому наши
                        дизайнеры сделают его узнаваемым и неповторимым
                    </div>
                </div>
                <div class="step d-flex flex-column">
                    <div class="hitman">
                        <object data="/img/image.svg" type=""></object>
                        <div class="text">Проведем тематическую фотосессию</div>
                    </div>
                    <div class="text2">
                        Также как и дизайн, фотоконтент имеет неоспоримое влияение на вовлечение подписчиков и
                        дальнейшие продажи вашего продукта или услуг
                    </div>
                </div>
                <div class="step d-flex flex-column">
                    <div class="hitman">
                        <object data="/img/bullhorn.svg" type=""></object>
                        <div class="text">Настроим таргетированную рекламу</div>
                    </div>
                    <div class="text2">
                        В нашем агенстве работают только сертифицированные специалисты по таргетированной рекламе.
                        Поэтому вам не стоит переживать за качество рекламных кампаний
                    </div>
                </div>
                <div class="step d-flex flex-column">
                    <div class="hitman">
                        <object data="/img/presentation.svg" type=""></object>
                        <div class="text">Подключим собственную систему аналитики</div>
                    </div>
                    <div class="text2">
                        Мы разработали собственную систему аналитики, направленную на увеличение качества работ с нашими
                        клиентами. Сервис закрыт для сторонних пользователей, им могут пользоваться только наши клиенты
                    </div>
                </div>
                <div class="step d-flex flex-column">
                    <div class="hitman">
                        <object data="/img/funnel.svg" type=""></object>
                        <div class="text">Настроим полноценную воронку продаж через CRM</div>
                    </div>
                    <div class="text2">
                        Рабочая воронка продаж - путь к пониманю эффективности этапов продаж. Мы знаем, как пользоваться
                        этим инструментом и настроим его для вас, после чего обучим им пользоваться. Также подключим
                        собственный бот для моментального приема лидов в Telegram
                    </div>
                </div>
                <div class="step d-flex flex-column">
                    <div class="hitman">
                        <object data="/img/flash.svg" type=""></object>
                        <div class="text">Проведем активации</div>
                    </div>
                    <div class="text2">
                        За годы работы с умной лентов мы разработали собственный алгоритм активации сообществ в
                        соц.сетях, который включает в себя проведение конкурсов и работу с блогерами. Данный алгоритм
                        нацелен на “укрощение” умной ленты в соц.сетях
                    </div>
                </div>
                <div class="step d-flex flex-column">
                    <div class="hitman">
                        <object data="/img/instagram2.svg" type=""></object>
                        <div class="text">Будем модерировать соц.сети</div>
                    </div>
                    <div class="text2">
                        Быстрый и компетентный ответ на вопрос потенциального покупателя в соц.сетях - залог увеличения
                        процента продаж из обращений. Каждая пройденная минута - риск потери клиента. Наши менеджеры
                        моментально реагируют на все комментарии и сообщения, а также используют скрипты продаж для
                        успешного “закрытия” лида
                    </div>
                </div>
            </div>
            <div class="container daniar d-flex align-items-center">
                <img src="{{ asset('img/daniar.png') }}" alt="" loading="lazy">
                <div class="d-flex flex-column">
                    <div class="big-txt">ЛЮДИ, ОТ КОТОРЫХ ЗАВИСИТ РЕЗУЛЬТАТ!</div>
                    <div class="small-txt">Вам везде предлагают кучу клиентов за неделю и рассказывают об одинаковых
                        процессах и инструментах, но никто не рассказывает о кадрах, которые будут работать над вашим
                        продуктом. А ведь от этих людей и их действий зависит 99% успеха вашего продукта в социальных
                        сетях!
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="head-title">
                    НАД КАЖДЫМ ПРОЕКТОМ ТРУДИТСЯ <br> <span>КОМАНДА СПЕЦИАЛИСТОВ</span>
                </div>
                <div class="team d-flex flex-wrap justify-content-between">
                    <div class="item">
                        <div class="big">12 дизайнеров</div>
                        <div class="small">Выполнение задач по разработке фирменного стиля, прорисовке продающих
                            баннеров (согласно ТЗ от таргетологов), оформлению контента для соц.сетей.
                        </div>
                    </div>
                    <div class="item">
                        <div class="big">25 таргетологов</div>
                        <div class="small">Настройка таргетированной рекламы, написание продающих текстов для рекламных
                            креативов (часть данной работы выполняется копирайтерами), анализ рекламных кампаний с
                            помощью сервисов аналитики.
                        </div>
                    </div>
                    <div class="item">
                        <div class="big">8 специалистов по SERMу</div>
                        <div class="small">Работа с репутацией бренда не только в социальных медиа, но и на всех
                            возможных форумах и прочих интернет-ресурсах.
                        </div>
                    </div>
                    <div class="item">
                        <div class="big">5 директологов</div>
                        <div class="small">Настройка контекстной рекламы в Google ADS и Яндекс.Дирекет. Аналитика
                            эффективности проведенных РК
                        </div>
                    </div>
                    <div class="item">
                        <div class="big">7 фотографов</div>
                        <div class="small">Проведение фотосъемок согласно особенностями продукта или услуги, а также
                            согласно ТЗ от Project-менеджера.
                        </div>
                    </div>
                    <div class="item">
                        <div class="big">36 копирайторов</div>
                        <div class="small">Написание текстов для постов в сообществах, а также рекламных постов.</div>
                    </div>
                    <div class="item">
                        <div class="big">28 Project-менеджеров</div>
                        <div class="small">Постановка задач по проекту, контроль выполнения задач, коммуникация с
                            клиентом.
                        </div>
                    </div>
                    <div class="item">
                        <div class="big">5 программистов</div>
                        <div class="small">Решение проблем с сайтами клиентов, установка всех необходимых программных
                            решений.
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="head-title">
                    НАШ ОТДЕЛ ПРОДВИЖЕНИЯ <span>ОРИЕНТИРУЕТСЯ</span> <br> ТОЛЬКО <span>НА ПРОДАЖИ</span>, ОСТАЛЬНОЕ -
                    ФИКЦИЯ!
                </div>
                <div class="sales d-flex flex-column">
                    <div class="d-flex flex-lg-row flex-column justify-content-between align-items-center sale">
                        <div class="text-block">
                            <div class="big"><span>СОБИРАЕМ</span> УЗКИЕ ТАРГЕТ-ГРУППЫ</div>
                            <div class="small">В рамках рекламной кампании мы тестируем до 100 различных гипотез в
                                квартал. Для создания аудиторий мы используем сервисы-шпионы, чтобы проанализировать
                                работу конкурентов, Яндекс Метрику и Google Analytics, чтобы увидеть максимально точный
                                портрет клиентов, создаем look-a-like аудитории с помощью частных баз и внутренних
                                инструментов рекламных кабинетов.
                            </div>
                        </div>
                        <img src="{{ asset('img/1site.gif') }}" alt="" loading="lazy">
                    </div>
                    <div class="d-flex flex-lg-row flex-column justify-content-between align-items-center sale">
                        <img src="{{ asset('img/2site.gif') }}" alt="" loading="lazy">
                        <div class="text-block">
                            <div class="big"><span>ПОДГОТАВЛИВАЕМ</span> ПРОДАЮЩИЕ ОБЪЯВЛЕНИЯ</div>
                            <div class="small">За годы работы у нас выработался алгоритм по составлению продающих
                                рекламных объявлений. Наработаны
                                собственные продающие "фишки" и правила их применения в зависимости от ниши клиента.
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-lg-row flex-column justify-content-between align-items-center sale">
                        <div class="text-block">
                            <div class="big">ИСПОЛЬЗУЕМ МЕТОД <br> <span>"СТАДНОГО ИНСТИНКТА"</span></div>
                            <div class="small">Какому объявлению вы поверите больше: у которого 0 лайков и 0
                                комментариев? Или может у которого 700 лайков и 15 хвалебных отзывов о качестве и
                                эффективности услуги, а также закрыты все страхи и возражения в комментариях? Мы умеем
                                работать над эффективностью объявлений!
                            </div>
                        </div>
                        <img src="{{ asset('img/3site.gif') }}" alt="" loading="lazy">
                    </div>
                    <div class="d-flex flex-lg-row flex-column justify-content-between align-items-center sale">
                        <img src="{{ asset('img/4site.gif') }}" alt="" loading="lazy">
                        <div class="text-block">
                            <div class="big"><span>РАБОТАЕМ</span> С РЕКЛАМОЙ В <br> РЕАЛЬНОМ ВРЕМЕНИ</div>
                            <div class="small">Подготовить аудитории с креативами и запустить их в работу это даже не
                                половина успеха. В первые дни запуска рекламных кампаний мы занимаемся постоянной
                                аналитикой РК, вносим корректировки и отключаем неэффективные объявления и аудитории.
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-lg-row flex-column justify-content-between align-items-center sale">
                        <div class="text-block">
                            <div class="big"><span>ИНСТРУМЕНТЫ</span> АНАЛИТИКИ <br> - НАШ ЛУЧШИЙ ДРУГ</span></div>
                            <div class="small">Какому объявлению вы поверите больше: у которого 0 лайков и 0
                                комментариев? Или может у которого 700 лайков и 15 хвалебных отзывов о качестве и
                                эффективности услуги, а также закрыты все страхи и возражения в комментариях? Мы умеем
                                работать над эффективностью объявлений!
                            </div>
                        </div>
                        <img src="{{ asset('img/5site.gif') }}" alt="" loading="lazy">
                    </div>
                </div>
            </div>
            <div class="saplya d-flex flex-column align-items-center position-relative">
                <h3>Почему нужно с нами <span>связаться</span>?</h3>
                <div class="d-flex flex-lg-row flex-column justify-content-between steps">
                    <div class="d-flex flex-column justify-content-between left">
                        <div class="d-flex align-items-center">
                            <p class="big-txt">1</p>
                            <p class="small-txt">Можно совершенно бесплатно пообщаться с профессиональным
                                специалистом</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">2</p>
                            <p class="small-txt">После понимания того, что вам нужно, мы подготовим подробный медиаплан
                                и стратегию продвижения</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">3</p>
                            <p class="small-txt">Разработаем индивидуальную концепцию вашего аккаунта</p>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-between right">
                        <div class="d-flex align-items-center">
                            <p class="big-txt">4</p>
                            <p class="small-txt">Проведем совместный брейншторм в Zoom или Skype</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">5</p>
                            <p class="small-txt">Расскажите о более чем 20 новых инструментах продвижения</p>
                        </div>
                        <div class="d-flex align-items-center">
                            <p class="big-txt">6</p>
                            <p class="small-txt">Перед тем, как запуститься, пропишем обязательства в договоре </p>
                        </div>
                    </div>
                </div>
                <a href="" class="take-plan" data-bs-toggle="modal" data-bs-target="#modalRequest">
                    <div>
                        Получить
                        <br>
                        план
                    </div>
                </a>
            </div>
        </section>
        <section class="price-uslug position-relative">
            <div class="container">
                <h3 data-aos="fade-right" data-aos-duration="1000">Стоимость услуг</h3>
                <div class="d-flex flex-lg-row flex-column align-items-start justify-content-between uslugi">
                    <div class="d-flex flex-column box" data-aos="zoom-in-up" data-aos-duration="1000">
                        <h4>Консультация</h4>
                        <p class="price">От 80 000 тенге</p>
                        <p>Поговорим о вашем проекте, обсудим самые разные задачи и найдем решение. В процессе вы
                            получите экспресс-анализ вашего бизнеса, ответы на вопросы, а также план действий на
                            будущее.</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Оставить заявку
                            </div>
                        </a>
                    </div>
                    <div class="d-flex flex-column box" data-aos="zoom-in-up" data-aos-duration="1000">
                        <h4>Можно бесплатно?</h4>
                        <p>Да, можно бесплатно получить у нас консультацию: поговорим о вашем проекте, обсудим самые
                            разные задачи и найдем решения. В процессе вы получите экспресс-анализ вашего маркетинга,
                            ответы на вопросы, а также план действий</p>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalRequest">
                            <div>
                                Записаться на консультацию
                            </div>
                        </a>
                    </div>
                </div>
                <h3>Актуальные кейсы:</h3>
                <div class="d-flex flex-lg-row flex-column justify-content-between">
                    <div class="boxiki" data-aos="zoom-in-right" data-aos-duration="1000"></div>
                    <div class="boxiki" data-aos="zoom-in-up" data-aos-duration="1000"></div>
                    <div class="boxiki" data-aos="zoom-in-left" data-aos-duration="1000"></div>
                </div>
                <div class="line">

                </div>
            </div>
            <div class="kara-shapan"></div>
        </section>
    </div>
@endsection
