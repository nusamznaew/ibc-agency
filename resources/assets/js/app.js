require('./bootstrap');

function customCursor(options) {
    let settings = $.extend({
            targetClass: 'custom-cursor', // create element with this class
            wrapper: $('body'), // jQuery
            speed: .5,
            movingDelay: 300, // fire event onStop after delay
            hasHover: false, // has hover events
            hoverTarget: $('a[href], button'),
            touchDevices: false, // show on touch devices
            onMove: function(data) {}
        }, options),
        data = {},
        checkTouch = !settings.touchDevices && "undefined" !== typeof document.documentElement.ontouchstart,
        timer = null;

    // exit
    if (checkTouch || !settings.wrapper.length) return;

    // append the ball
    settings.wrapper.append(`<div class="${settings.targetClass}"></div>`);

    let $cursor = $('.' + settings.targetClass),
        position = {
            x: window.innerWidth / 2,
            y: window.innerHeight / 2
        },
        mouse = {
            x: position.x,
            y: position.y
        },
        setX = gsap.quickSetter($cursor, "x", "px"),
        setY = gsap.quickSetter($cursor, "y", "px");

    // up<a href="https://www.jqueryscript.net/time-clock/">date</a> data
    data.cursor = $cursor;

    // on mouse move
    window.addEventListener("mousemove", init);

    function init() {
        // remove default mousemove event
        window.removeEventListener("mousemove", init);

        // add new custom event
        window.addEventListener("mousemove", e => {
            mouse.x = e.x;
            mouse.y = e.y;

            // update data and trigger event
            data.isMoving = true;
            settings.onMove(data);

            timer = setTimeout(function() {
                // update data and trigger event
                data.isMoving = false;
                settings.onMove(data);
            }, settings.movingDelay);
        });

        // fade out cursor
        document.addEventListener("mouseleave", e => {
            // update data and trigger event
            data.isInViewport = false;
            settings.onMove(data);
        });

        // update cursor's position
        document.addEventListener("mouseenter", e => {
            mouse.x = position.x = e.x;
            mouse.y = position.y = e.y;

            // update data and trigger event
            data.isInViewport = true;
            settings.onMove(data);
        });

        gsap.ticker.add((time, deltaTime) => {
            let fpms = 60 / 1000,
                delta = deltaTime * fpms,
                dt = 1 - Math.pow(1 - settings.speed, delta);
            position.x += (mouse.x - position.x) * dt;
            position.y += (mouse.y - position.y) * dt;
            setX(position.x);
            setY(position.y);
        });

        data.isInViewport = true;
    }

    // on hover
    if (settings.hasHover && settings.hoverTarget.length) {
        setTimeout(function() {
            settings.hoverTarget.hover(function() {
                data.hoverTarget = $(this);
                data.isHover = true;
                settings.onMove(data);
            }, function() {
                data.hoverTarget = $(this);
                data.isHover = false;
                settings.onMove(data);
            });
        }, 100);
    }
}

// big ball
customCursor({
    hasHover: true,
    onMove: function(data) {
        if (data.isInViewport) {
            // in viewport
            if (data.isMoving) {
                if (data.isHover) {
                    gsap.to(data.cursor, {
                        opacity: 1,
                        scale: .3,
                        background: '#000000'
                    });
                } else {
                    gsap.to(data.cursor, {
                        opacity: .5,
                        scale: .8,
                        background: 'transparent'
                    });
                }
            } else {
                if (data.isHover) {
                    gsap.to(data.cursor, {
                        opacity: 1,
                        scale: .3,
                        background: '#000000'
                    });
                } else {
                    gsap.to(data.cursor, {
                        opacity: .5,
                        scale: 1,
                        background: 'transparent'
                    });
                }
            }
        } else {
            // out viewport
            gsap.to(data.cursor, {
                opacity: 0,
                scale: 0,
                background: 'transparent'
            });
        }
    },
});

// dot inside
customCursor({
    targetClass: 'custom-cursor-dot',
    speed: .5,
    onMove: function(data) {
        if (data.isInViewport) {
            gsap.to(data.cursor, {
                opacity: 1
            });
        } else {
            gsap.to(data.cursor, {
                opacity: 0
            });
        }
    },
});

function magneticButton(options) {
    let settings = $.extend({
            target: $('[data-magnetic]'), // jQuery element
            class: 'magnetizing',
            attraction: 0.45, // 1 is weak, 0 is strong
            distance: 100, // magnetic area around element
            onEnter: function(data) {},
            onExit: function(data) {},
            onUpdate: function(data) {},
        }, options),

        isEnter = false,

        // distance from mouse to center of target
        distanceFromMouse = function($target, mouseX, mouseY) {
            let centerX = $target.offset().left + $target.outerWidth() / 2,
                centerY = $target.offset().top + $target.outerHeight() / 2,
                pointX = mouseX - centerX,
                pointY = mouseY - centerY,
                distance = Math.sqrt(Math.pow(pointX, 2) + Math.pow(pointY, 2));

            return Math.floor(distance);
        },

        // processing
        magnetize = function($this, e) {
            let mouseX = e.pageX,
                mouseY = e.pageY;

            $this.each(function() {
                let $this = $(this),
                    centerX = $this.offset().left + $this.outerWidth() / 2,
                    centerY = $this.offset().top + $this.outerHeight() / 2,
                    deltaX = Math.floor(centerX - mouseX) * -1 * settings.attraction,
                    deltaY = Math.floor(centerY - mouseY) * -1 * settings.attraction,
                    mouseDistance = distanceFromMouse($this, mouseX, mouseY),
                    data = {
                        target: $this,
                        y: deltaY,
                        x: deltaX,
                        distance: mouseDistance
                    };

                if (mouseDistance < settings.distance) {
                    gsap.to($this, {
                        y: deltaY,
                        x: deltaX
                    });

                    // enter
                    if (!isEnter) {
                        isEnter = true;
                        $this.addClass(settings.class);
                        settings.onEnter(data);
                    }

                    // update
                    settings.onUpdate(data);
                } else {
                    gsap.to($this, {
                        y: 0,
                        x: 0
                    });

                    // exit
                    if (isEnter) {
                        isEnter = false;
                        $this.removeClass(settings.class);
                        settings.onExit(data);
                    }
                }
            });
        };

    // exit
    if (!settings.target.length) return;

    // on mouse move
    $(window).on('mousemove', function(e) {
        magnetize(settings.target, e);
    });
}

// init
magneticButton({
    distance: 120,
    onEnter: function(data) {
        //gsap.to(data.target, {scale: 1.2});
        console.log(data);
    },
    onExit: function(data) {
        //gsap.to(data.target, {scale: 1});
        console.log(data);
    },
    onUpdate: function(data) {
        console.log(data);
    }
});
