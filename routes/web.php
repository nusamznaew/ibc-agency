<?php

use App\Http\Controllers\MailController;
use App\Http\Controllers\VacancyController;
use App\Http\Controllers\WorkController;
use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main-page');
})->name('main-page');

Route::get('/vacancy', [VacancyController::class, 'index'])->name('vacancy');
Route::post('/vacancy/send', [VacancyController::class, 'send'])->name('vacancy.send');

Route::post('/form/send', [MailController::class, 'send'])->name('form.send');

Route::get('/works', [WorkController::class, 'index'])->name('works.index');
Route::get('/works/{work}', [WorkController::class, 'show'])->name('works.show');
Route::post('/works/send', [WorkController::class, 'send'])->name('works.send');

Route::get('/decision', function () {
    return view('decision');
})->name('decision');


Route::get('/target', function () {
    return view('target');
})->name('target');


Route::get('/web-dev', function () {
    return view('web-dev');
})->name('web.dev');


Route::get('/marketing', function () {
    return view('marketing');
})->name('marketing');

Route::get('/success', function () {
    return view('success');
})->name('success');

//Clear Cache
Route::get('/clear_cache', function () {

    \Artisan::call('optimize');

    dd("Cache is cleared");
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
