<?php

namespace App\Http\Controllers;

use App\Mail\SendLead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class MailController extends Controller
{
    public function send(Request $request)
    {

        $data = [
            'lead_name' => $request->lead_name,
            'lead_phone' => $request->lead_phone,
            'lead_company' => $request->lead_company,
        ];

        $to = 'ibc.agency@yandex.kz';
		$to2 = 'zholdasbayev.daniyar@gmail.com';

        Mail::to($to)->send(new SendLead($data));
		Mail::to($to2)->send(new SendLead($data));

        return redirect()->route('success');
    }
}
