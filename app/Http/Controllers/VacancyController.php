<?php

namespace App\Http\Controllers;

use App\Mail\SendVacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class VacancyController extends Controller
{
    public function index()
    {
        return view('vacancy');
    }

    public function send(Request $request)
    {

        $data = [
            'name' => $request->name,
            'position' => $request->position,
            'phone' => $request->phone,
            'social' => $request->social,
            'description' => $request->description,
            'file' => $request->file('file')
        ];

        $to = 'ibc.agency@yandex.kz';
		$to2 = 'zholdasbayev.daniyar@gmail.com';

        Mail::to($to)->send(new SendVacancy($data));
		Mail::to($to2)->send(new SendVacancy($data));

        return redirect()->route('success');
    }
}
