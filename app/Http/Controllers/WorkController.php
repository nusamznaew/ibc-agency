<?php

namespace App\Http\Controllers;

use App\Mail\SendWork;
use App\Models\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WorkController extends Controller
{
    public function index()
    {
        $works = Work::orderBy('created_at', 'desc')->get();

        return view('works')->with('advices', $works);
    }

    public function show($slug)
    {
        $work = Work::where('slug', $slug)->firstOrFail();

        return view('work')->with([
            'work' => $work,
        ]);
    }

    public function send(Request $request)
    {

        $data = [
            'work_name' => $request->work_name,
            'work_phone' => $request->work_phone,
            'work_description' => $request->work_description,
        ];

        $to = 'ibc.agency@yandex.kz';
		$to2 = 'zholdasbayev.daniyar@gmail.com';

        Mail::to($to)->send(new SendWork($data));
		Mail::to($to2)->send(new SendWork($data));

        return redirect()->route('success');
    }
}
