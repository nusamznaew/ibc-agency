<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendWork extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $work_name = $this->data['work_name'];
        $work_phone = $this->data['work_phone'];
        $work_description = $this->data['work_description'];

        return $this->subject('Заявка на консультацию')
            ->view('mail.work-mail', compact('work_name', 'work_phone', 'work_description'));
    }
}
