<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendLead extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $lead_name = $this->data['lead_name'];
        $lead_phone = $this->data['lead_phone'];
        $lead_company = $this->data['lead_company'];

        return $this->subject('Заявка на консультацию')
            ->view('mail.lead-mail', compact('lead_name', 'lead_phone', 'lead_company'));
    }
}
